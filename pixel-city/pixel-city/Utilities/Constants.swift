//
//  Constants.swift
//  pixel-city
//
//  Created by LuckCornKoo on 5/29/18.
//  Copyright © 2018 NguyenQuocCuong. All rights reserved.
//

import Foundation

let DROP_PIN_ID = "droppablePin"
let PULL_UP_VIEW_MAXHEIGHT = 300.0
let REGION_RADIUS = 1000.0

let PHOTO_CELL_ID = "photoCell"

//API Key
let API_KEY = "01f58a4fd2c7492a59362b0a183bc784"

//URL
//let URL_PHOTO = "\(farm)"

//Global Functions
func flickrURL(forAPIKey key: String, withAnnotation annotation: DroppablePin, andNumberOfPhotos number: Int) -> String{
    return "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(key)&lat=\(annotation.coordinate.latitude)&lon=\(annotation.coordinate.longitude)&radius=1&radius_units=mi&per_page=\(number)&format=json&nojsoncallback=1"
}
