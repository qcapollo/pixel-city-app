//
//  FlickrPhoto.swift
//  pixel-city
//
//  Created by LuckCornKoo on 5/30/18.
//  Copyright © 2018 NguyenQuocCuong. All rights reserved.
//

//Photo's Sizes
// _q_d : for square size (150 x 150)
// _m_d : for small size (240 x ???)
// _z_d : for medium size (640 x ???)
// _k_d : for large size (2048 x ???)
// _o_d : for original size
// These options above can't be used with "_secret" value we got from the JSON result!
// Only these options
// _c_d : for medium size(800 x ???)
// _b_d : for large size(1024 x ???)
import Foundation

class FlickrPhoto {
    private(set) var _farm: AnyObject
    private(set) var _id: AnyObject
    private(set) var _owner: AnyObject
    private(set) var _secret: AnyObject
    private(set) var _server: AnyObject
    
    init(farm: AnyObject, id: AnyObject, owner: AnyObject, secret: AnyObject, server: AnyObject) {
        self._farm = farm
        self._id = id
        self._owner = owner
        self._secret = secret
        self._server = server
    }
    func getDownloadLink() -> String {
        let result = "https://farm\(_farm).staticflickr.com/\(_server)/\(_id)_\(_secret)_b_d.jpg" //original size
        return result
    }
}
