//
//  PopUpVC.swift
//  pixel-city
//
//  Created by LuckCornKoo on 5/30/18.
//  Copyright © 2018 NguyenQuocCuong. All rights reserved.
//

import UIKit

class PopUpVC: UIViewController, UIGestureRecognizerDelegate {
    //Outlets
    @IBOutlet weak var popImageView: UIImageView!
    
    var image: UIImage!
    
    func initData(forImage image: UIImage){
        self.image = image
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        popImageView.image = self.image
        addDoubleTap()
        UIApplication.shared.statusBarStyle = .lightContent
    }
    private func addDoubleTap(){
        let dbTap = UITapGestureRecognizer(target: self, action: #selector(dismissView))
        dbTap.numberOfTapsRequired = 2
        dbTap.delegate = self
        self.view.addGestureRecognizer(dbTap)
    }
    @objc func dismissView(){
        self.dismiss(animated: true) {
        }
    }

}
