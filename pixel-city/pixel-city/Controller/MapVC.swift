//
//  ViewController.swift
//  pixel-city
//
//  Created by LuckCornKoo on 5/28/18.
//  Copyright © 2018 NguyenQuocCuong. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Alamofire
import AlamofireImage

class MapVC: UIViewController, UIGestureRecognizerDelegate {
    //Outlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var pullUpView: UIView!
   
    @IBOutlet weak var pullUpViewHeightConstraint: NSLayoutConstraint!
    
    var screenSizeBounds = UIScreen.main.bounds
    var spinner: UIActivityIndicatorView?
    var progressLbl: UILabel?
    
    //CollectionView for Flickr Images
    var flowLayout =  UICollectionViewFlowLayout()
    var collectionView: UICollectionView?
    
    //URLArray
    var photoURLArray = [String]()
    
    //PhotoArray
    var photoArray = [UIImage]()
    
    //Variables
    var locationManager = CLLocationManager()
    var authorizationStatus = CLLocationManager.authorizationStatus()
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        mapView.showsUserLocation = true
        locationManager.delegate = self
        
        configureLocationServices()
        //
        addDoubleTap()
        addSwipeDownGesture()
        //
        configureCollectionView()
        
        registerForPreviewing(with: self, sourceView: collectionView!)
        
        pullUpView.addSubview(collectionView!)
    }
    override func viewDidAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    //Gestures
    private func addDoubleTap() {
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(dropPin(sender:)))
        doubleTap.numberOfTapsRequired = 2
        doubleTap.delegate = self
        mapView.addGestureRecognizer(doubleTap)
    }
    private func addSwipeDownGesture(){
        let pullDown = UISwipeGestureRecognizer(target: self, action: #selector(animateViewDown))
        pullDown.direction = .down
        pullDown.delegate = self
//        mapView.addGestureRecognizer(pullDown)
        pullUpView.addGestureRecognizer(pullDown)
    }
    //Animating
    func animateViewUp(){
        if pullUpViewHeightConstraint.constant == 0.0 {
            pullUpViewHeightConstraint.constant = CGFloat(PULL_UP_VIEW_MAXHEIGHT)
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
    @objc private func animateViewDown(){
        cancelAllSessions()
        if pullUpViewHeightConstraint.constant > 0.0 {
            pullUpViewHeightConstraint.constant = 0.0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
    private func addSpinner(){
        spinner = UIActivityIndicatorView()
        spinner?.center = CGPoint(x: screenSizeBounds.width / 2 - (spinner?.frame.width)! / 2, y: CGFloat(PULL_UP_VIEW_MAXHEIGHT / 2 - 25))
        spinner?.activityIndicatorViewStyle = .whiteLarge
        spinner?.color = #colorLiteral(red: 1, green: 0.6632423401, blue: 0, alpha: 1)
        spinner?.startAnimating()
        collectionView?.addSubview(spinner!)
    }
    private func removeSpinner(){
        if spinner != nil {
            spinner?.removeFromSuperview()
        }
    }
    private func addProgressLbl(){
        progressLbl = UILabel()
        progressLbl?.frame = CGRect(x: screenSizeBounds.width / 2 - 120, y: CGFloat(PULL_UP_VIEW_MAXHEIGHT / 2 + 10), width: 240, height: 40)
        progressLbl?.font = UIFont(name: "Avenir Next", size: 18)
        progressLbl?.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        progressLbl?.textAlignment = .center
        collectionView?.addSubview(progressLbl!)
        //debug
        self.progressLbl?.text = ""
    }
    private func removeProgressLbl(){
        if progressLbl != nil {
            progressLbl?.removeFromSuperview()
        }
    }
    func retrieveUrls(forAnnotation annotation: DroppablePin, completion: @escaping (_ status: Bool) -> ()){
        photoURLArray.removeAll()
        Alamofire.request(flickrURL(forAPIKey: API_KEY, withAnnotation: annotation, andNumberOfPhotos: 40)).responseJSON { (response) in
            if (response.result.error == nil){
                guard let json = response.result.value as? Dictionary<String, AnyObject> else {return}
                let photosDict = json["photos"] as! Dictionary<String, AnyObject>
                let photosArray = photosDict["photo"] as! [Dictionary<String, AnyObject>]
                for item in photosArray {
                    let newPhoto = FlickrPhoto(farm: item["farm"]!, id: item["id"]!, owner: item["owner"]!, secret: item["secret"]!, server: item["server"]!)
                    self.photoURLArray.append(newPhoto.getDownloadLink())
//                    print(self.photoURLArray[self.photoURLArray.endIndex - 1])
                }
                completion(true)
            }
            else {
                print(response.result.debugDescription)
                completion(false)
                
            }
        }
    }
    func retrievePhotos(completion: @escaping (_ status: Bool) -> ()){
        
        photoArray.removeAll()
        for url in photoURLArray {
            Alamofire.request(url).responseImage { (response) in
                if response.result.error == nil {
//                    debugPrint(response)
//
//                    print(response.request)
//                    print(response.response)
//                    debugPrint(response.result)
                    
                    if let newImage = response.result.value {
                        self.photoArray.append(newImage)
                        self.progressLbl?.text = "\(self.photoArray.count) of 40 photos downloaded"
                    }
                    if self.photoArray.count == self.photoURLArray.count {
                        completion(true)
                    }
                }
                else {
                    print("Error when download image!")
                    completion(false)
                }
            }
        }
        //These codes below are not work - I don't know why!
//        if self.photoArray.count == self.photoURLArray.count {
//            completion(true)
//        }
    }
    
    func cancelAllSessions(){ Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
            sessionDataTask.forEach({ $0.cancel() })
            downloadData.forEach({ $0.cancel() })
        }
    }
    //Actions Btn
    @IBAction func centerMapBtnPressed(_ sender: Any) {
        if authorizationStatus == .authorizedAlways || authorizationStatus == .authorizedWhenInUse {
            centerMapOnUserLocation()
        }
    }
}


extension  MapVC: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        
        let pinAnnotation = MKPinAnnotationView(annotation: annotation, reuseIdentifier: DROP_PIN_ID)
        pinAnnotation.pinTintColor = #colorLiteral(red: 0.9771530032, green: 0.7062081099, blue: 0.1748393774, alpha: 1)
        pinAnnotation.animatesDrop = true
        return pinAnnotation
    }
    
    func centerMapOnUserLocation(){
        guard let coordinate = locationManager.location?.coordinate else {return}
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(coordinate, REGION_RADIUS * 2.0, REGION_RADIUS * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    @objc func dropPin(sender: UITapGestureRecognizer){
        //Remove old Data
        removeSpinner()
        removeProgressLbl()
        mapView.removeAnnotations(mapView.annotations)
        cancelAllSessions()
        photoURLArray.removeAll()
        photoArray.removeAll()
        collectionView?.reloadData()
        //
        addSpinner()
        addProgressLbl()
        
        //Generate data for new annotation
        let touchPoint = sender.location(in: mapView)
        let touchCoordinate = mapView.convert(touchPoint, toCoordinateFrom: mapView)
        let annotation = DroppablePin(coordinate: touchCoordinate, identifier: DROP_PIN_ID)
        
        //Re-centered added annotation
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(touchCoordinate, REGION_RADIUS * 2.0, REGION_RADIUS * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
        
        //Add annotation into mapView
        mapView.addAnnotation(annotation)
        
        animateViewUp()
        retrieveUrls(forAnnotation: annotation) { (success) in
            if success {
//                print(self.photoURLArray)
//                print(self.photoURLArray.count)
                //Download photos!
                self.retrievePhotos(completion: { (finished) in
                    if finished {
                        //hide spinner
                        self.removeSpinner()
                        //hide label
                        self.removeProgressLbl()
                        //reload collectionView
                        self.collectionView?.reloadData()
                    }
                })
            }
        }
        
    }
}
extension MapVC: CLLocationManagerDelegate {
    func configureLocationServices(){
        if authorizationStatus == .notDetermined {
            locationManager.requestAlwaysAuthorization()
        }
        else {return}
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        centerMapOnUserLocation()
    }
}


extension MapVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // number of items in array
        return self.photoArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PHOTO_CELL_ID, for: indexPath) as? PhotoCell else {
            return UICollectionViewCell()
        }
        let imageFromIndex = photoArray[indexPath.row]
        let photoView = UIImageView(image: imageFromIndex)
        cell.addSubview(photoView)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let popVC = storyboard?.instantiateViewController(withIdentifier: "PopVC") as? PopUpVC else {return}
        popVC.initData(forImage: photoArray[indexPath.row])
        present(popVC, animated: true)
    }
    func configureCollectionView(){
        flowLayout.scrollDirection = .vertical
        
        collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: flowLayout)
        collectionView?.register(PhotoCell.self, forCellWithReuseIdentifier: PHOTO_CELL_ID)
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.backgroundColor = #colorLiteral(red: 0.9699201236, green: 0.9872218505, blue: 1, alpha: 1)
    }
    
}

extension MapVC: UIViewControllerPreviewingDelegate {
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let indexPath = collectionView?.indexPathForItem(at: location), let cell = collectionView?.cellForItem(at: indexPath) else {return nil}
        guard let popVC = storyboard?.instantiateViewController(withIdentifier: "PopVC") as? PopUpVC else {return nil}
        popVC.initData(forImage: photoArray[indexPath.row])
        
        previewingContext.sourceRect = cell.contentView.frame
        return popVC
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        show(viewControllerToCommit, sender: self)
    }
    
    
}
